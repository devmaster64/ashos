#!/bin/bash

if ! lsmod | grep "loop" &> /dev/null ; then
    echo 'Loop device kernel module not loaded.'
    echo 'Please run `modprobe loop` as root.'
    exit 1
fi

set -e

TOOLSDIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )
IMGDIR=$TOOLSDIR/../img
OUTDIR=$TOOLSDIR/../out
DISKIMG=$IMGDIR/disk.img

echo -e "\e[93;1mDeleting ${IMGDIR}\e[0m"
rm -rf $IMGDIR
mkdir -p $IMGDIR

echo -e "\e[93;1mCreating 256MB image file\e[0m"
dd if=/dev/zero of=$DISKIMG bs=1M count=256 

echo -e "\e[93;1mCreating msdos partition table and 1 partition in it.\e[0m"
printf 'o\nn\np\n\n\n\na\nw\nq\n' | fdisk $DISKIMG

echo -e "\e[93;1mWriting stage 0 mbr boot code\e[0m"
dd if=$OUTDIR/bootloader/stage0.mbr of=$DISKIMG conv=notrunc

echo -e "\e[93;1mCreating loopback device for image file\e[0m"
sudo losetup -P /dev/loop0 $DISKIMG

echo -e "\e[93;1mFormatting partition with mkfs.ext2\e[0m"
sudo mkfs.ext2 /dev/loop0p1

echo -e "\e[93;1mWriting stage 1 ext2 boot block code\e[0m"
sudo dd if=$OUTDIR/bootloader/stage1.bootblock of=/dev/loop0p1 conv=notrunc

echo -e "\e[93;1mMounting ext2 partition at /mnt \e[0m"
sudo mount /dev/loop0p1 /mnt

echo -e "\e[93;1mCopying stage 2 bootloader\e[0m"
sudo cp $OUTDIR/bootloader/stage2.flat /mnt/.boot

echo -e "\e[93;1mUnmounting /mnt \e[0m"
sudo umount /mnt

echo -e "\e[93;1mDeleting loopback device\e[0m"
sudo losetup -D /dev/loop0