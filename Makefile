# Compiler related variables
CC = i386-elf-gcc
CC_INCLUDES = -I include/
CC_FLAGS = -g -ffreestanding $(CC_INCLUDES)
LD = i386-elf-ld
OBJCOPY = i386-elf-objcopy

# NASM related variables
ASM = nasm

# QEMU related flags
QEMU = qemu-system-i386
QEMU_FLAGS = -m 256

# Source folder
SRC = src
BOOTLOADER_SRC = $(SRC)/bootloader
OUT_DIR = out

all: clean bootloader disk-image
.PHONY: all

clean:
	$(MAKE) -C src/bootloader clean
	rm -rf $(OUT_DIR)

bootloader:
	mkdir -p $(OUT_DIR)
	$(MAKE) -C $(BOOTLOADER_SRC) build
	cp -r $(BOOTLOADER_SRC)/out/. $(OUT_DIR)/bootloader

disk-image:
	chmod +x ./tools/create-img.sh
	./tools/create-img.sh