gdt_start:
gdt_null:
    dd 0x0                            ; Null Descriptor
    dd 0x0
gdt_code:
    dw 0xffff                         ; Limit 4GB
    dw 0x0                            ; Base
    db 0x0                            ; Base
    db 10011010b                      ; Flags
    db 11001111b                      ; Flags then Limit
    db 0x0                            ; Base
gdt_data:
    dw 0xffff                         ; Limit 4GB
    dw 0x0                            ; Base
    db 0x0                            ; Base
    db 10010010b                      ; Flags
    db 11001111b                      ; Flags then Limit
    db 0x0                            ; Base
gdt_end:

gdt_descriptor:
  dw gdt_end - gdt_start - 1          ; Size of Descriptor
  dd gdt_start                        ; Pointer to GDT Start

code_segment equ gdt_code - gdt_start
data_segment equ gdt_data - gdt_start