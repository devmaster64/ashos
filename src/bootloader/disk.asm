; Contains routines related to disk operations

;-------------------------------------------------------------------------------
; reads sectors. check int 13,42
read_sectors:      
retry:
    mov si, packet
    mov ah, 0x42
    int 0x13
    jc read_error
    jmp done
read_error:    
    mov ah, 0x00      ;Reset diskdrive
    int 0x13
    cmp byte [retries_count], no_of_retires
    inc byte [retries_count]
    jbe retry
    mov si, disk_err_msg
    call print
    call halt
done:
    ret

no_of_retires equ 5
retries_count db 0
disk_err_msg db "b0: drive read fail", 0
;-------------------------------------------------------------------------------
; gets address of the active partition
get_vbr_packet:
    mov cx, 4
next:
    mov bx, part_table
    cmp byte [bx], 0x80            ; is boot drive?
    je success
    add bx, part_entry_size
    loop next
    mov si, active_err_msg
    call print
    call halt
success:
    mov dl, [boot_drive]
    mov word [packet], 0x0010
    mov word [packet+2], 2
    add bx, 0x8
    mov ebx, [ebx]
    mov dword [packet+8], ebx
    mov dword [packet+12], 0
    ret

part_table equ 0x7dbe
part_entry_size equ 16
active_err_msg db "b0: active partition missing", 0
