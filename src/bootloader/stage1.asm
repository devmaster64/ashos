; Stage 1 bootloader code
[bits 16]
[org 0x500]

xor eax, eax
xor ebx, ebx
xor ecx, ecx
xor edx, edx

pop dword [ext2_block_0_lba]        ; this currently points to the boot block
pop word [boot_drive]

; Assume that the block size of the ext2 partition is 1024 bytes.
; The group descriptor is at an offset of 2 block, i.e.  2048 bytes.
; We know that the sector size is 512 bytes, therefore we need to add 4 LBA to
; ext2_grp_desc_block

mov eax, [ext2_block_0_lba]
add eax, 4
mov dword [ext2_grp_desc_lba], eax   ; now this points to the group descripblock

pop word [read_sectors]          ; address of the read_sectors routine
pop word [print]                 ; address of the print routine

; Now we need to get the superblock

mov eax, 1                ; Read block 1
mov ebx, ext2_super_block            
call read_ext2_block

mov eax, dword 2
mov ebx, ext2_grp_desc_block
call read_ext2_block

mov dword eax, [ext2_grp_desc_block + 40]
mov ebx, 8
idiv ebx
mov dword [inode_table_block_size], eax

mov eax, dword [ext2_grp_desc_block+8]
mov ebx, inode_table
call read_ext2_block

; We skip the first 128 bytes which is inode 1
; Direct block pointer 0 is at offset 40
mov eax, dword [inode_table + 128 + 40] 
mov ebx, free_memory
call read_ext2_block

; free_memory contains the directory entries structure for `/`
; We now need to search for .boot file
mov ebx, free_memory
loop1:
    mov dword edx, [ebx]
    cmp edx, 0
    je stage2_fail
    mov byte al, [ebx + 7]
    cmp al, 1
    jne next
    mov al, [ebx + 6]
    cmp al, 5
    jne next
    xor ecx, ecx
    mov cl, al
    mov esi, stage2
    mov edi, ebx
    add di, 8
    repe cmpsb
    je success1    
next:
    add bx, [bx + 4]
    jmp loop1
success1:
    mov si, stage2_found
    call [print]   

    mov eax, edx
    mov bx, stage2_location
    call read_file

    ;entering protected mode
    cli
    xor eax, eax
    mov ds, eax
    lgdt [gdt_descriptor]
    mov eax, cr0
    or eax, 1
    mov cr0, eax
    
    jmp code_segment:protected_mode

stage2_fail:
    mov si, stage2_not_found_msg
    call [print]
halt:
    jmp $


read_file:
    push ebx
    mov ebx, free_memory
    call get_inode
    pop ebx
    mov edx, free_memory + 40
    mov ecx, 12
loop2:
    mov eax, [edx]
    cmp eax, 0
    je done
    call read_ext2_block
    add edx, 4    
    add ebx, 1024
    loop loop2
    mov si, stage2_size_err
    call [print]
    call halt
done:
    ret

get_inode:
    push edx
    xor edx, edx
    dec eax
    mov ecx, 8
    idiv ecx
    add eax, dword [ext2_grp_desc_block+8]
    push ebx
    mov ebx, inode_table
    call read_ext2_block
    mov eax, edx
    mov edx, 128
    mul edx
    mov esi, inode_table
    add esi, eax
    pop edi
    mov ecx, 128
    rep movsb
    pop edx
    ret
;-------------------------------------------------------------------------------
; read ext2 block
; arg0 = buffer address
; arg1 = block no
read_ext2_block:
    push edx
    mov dx, [boot_drive]
    mov byte [packet], 0x10
    mov byte [packet + 1], 0
    mov word [packet + 2], 2
    mov dword [packet + 4], ebx
    add eax, eax
    add eax, [ext2_block_0_lba]
    mov dword [packet + 8], eax
    mov dword [packet + 12], 0
    call [read_sectors]
    pop edx
    ret

%include "gdt.asm"

[bits 32]
protected_mode:
    mov ax, data_segment
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ebp, 0x90000
    mov esp, ebp
    call stage2_location
    call halt

stage2_location equ 0x900
free_memory equ 0x8e00
inode_table equ 0x8800
inode_table_block_size dd 0
ext2_super_block equ 0x8000
ext2_grp_desc_block equ 0x8400
ext2_grp_desc_lba dd 0
ext2_block_0_lba dd 0
stage2 db ".boot", 0
packet equ 0x7e00
greet_msg db "b1: loaded", 0
print dw 0
read_sectors dw 0
superblock dw 0
stage2_found db "b1: stage 2 bootloader found", 0
stage2_not_found_msg db "b1: stage 2 '/.boot' file not found", 0
boot_drive dw 0
stage2_size_err db "b1: stage 2 file size greater that 12 KiB. Halting...", 0
times 1024 - ($ - $$) db 0