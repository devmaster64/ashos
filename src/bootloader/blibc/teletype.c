#include <teletype.h>
#include <ports.h>

void _set_cursor_offset(u16 offset) {
    port_byte_out(REG_SCREEN_CTRL, 15);
    port_byte_out(REG_SCREEN_DATA, offset & 0xff);
    port_byte_out(REG_SCREEN_CTRL, 14);
    port_byte_out(REG_SCREEN_DATA, (offset >> 8) & 0xff);
}

u16 _get_cursor_offset() {
    port_byte_out(REG_SCREEN_CTRL, 15);
    u16 offset = 0;
    offset |= port_byte_in(REG_SCREEN_DATA);
    port_byte_out(REG_SCREEN_CTRL, 14);
    offset |= (port_byte_in(REG_SCREEN_DATA) << 8);
    return offset;
}

void _put_char(u8 ascii_char, u16 offset, vga_fb* fb)
{
    fb += offset;
    fb->ascii_character = ascii_char;
    fb->color_data = 7; // Light gray on black
}

u32 _print(str string, u32 offset) {
    u32 iter = 0;
    while(string[iter] != 0) {
        _put_char(string[iter], offset + iter, (vga_fb*)VGA_FB_ADDR);
        iter++;
    }
    return iter;
}

void print(str string) {
    u32 cursor_offset = _get_cursor_offset();
    u32 offset = _print(string, cursor_offset);
    u32 line_number = (cursor_offset + offset) / VGA_FB_COLS + 1;
    _set_cursor_offset(line_number * VGA_FB_COLS);
}