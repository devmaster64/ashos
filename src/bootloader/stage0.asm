; This is the stage 0 bootloader code

[bits 16]
[org 0x7c00]

; set up segments
xor ax, ax            ; set AX to 0
mov ds, ax            ; clear data segment
mov es, ax            ; clear extra segment
mov ax, stack_base
mov ss, ax            ; setting stack base
mov sp, stack_size    ; setting stack size
xor ax, ax

mov [boot_drive], dl  ; set by the BIOS, DL contains the startup disk

mov si, load_msg
call print

call enable_a20
call query_system_map
call get_vbr_packet             ; This fills the packet with the LBA number 
                                ; pointing to the ext2 boot block
mov word [packet+4], stage1     ; address where we want to copy the boot block
mov word [packet+6], 0          
call read_sectors               

; Arguments for our stage 1 routine
push word print                 ; address to the print routine
push word read_sectors          ; address to the read_sectors routine
push word [boot_drive]
push dword ebx                         ; LBA pointing to ext2 boot block
jmp stage1

%include "disk.asm"

;-------------------------------------------------------------------------------
; routines
;-------------------------------------------------------------------------------
; prints a string pointed by DS:SI
print:
    push ax         ; push AX onto stack because this routine uses it
    push cx         ; same with CX
    xor ax, ax
    xor cx, cx
next1:
    lodsb           ; read 1 byte from SI and store in AL
    test al, al     ; check if AL is NULL
    jz done1        ; jump to done if AL is NULL
    mov ah, 0xe     ; 0xe tele-prints character to screen
    int 0x10        ; 10h BIOS interrupt
    jmp next1       ; jump to next to print the next char
done1:
    mov al, 0xd     ; carriage Return
    int 0x10
    mov al, 0xa     ; line Feed
    int 0x10
    pop cx
    pop ax
    ret
;-------------------------------------------------------------------------------
; queries for system memory map
; returns address range descriptor array which is saved in ard_buffer
query_system_map:
    mov si, sysmap_msg
    call print
    mov edi, ard_buffer         ; pointer to our ARD array
    mov byte [ard_count], 0     ; counter for calculating array size
    xor ebx, ebx                ; setting continuation value to 0
next2:
    mov eax, 0xe820             ; queries system map
    mov ecx, ard_struct_size    ; size of each struct
    mov edx, smap               ; setting signature to "smap" = system map
    int 0x15
    jc error                    ; carry bit is set in case of error
    cmp eax, smap               ; should contain the string "smap"
    jne error                   ; if not its an error
    cmp ecx, ard_struct_size    ; confirming that its 20 bytes
    jne error                   ; if not its an error
    cmp ebx, 0                  ; 0 denotes end of array
    je done2                    ; so jump to done
    add edi, ecx                ; else we point to next free memory buffer
    inc byte [ard_count]        ; increase our array count
    jmp next2                   ; and read the next struct
error:
    jmp halt
done2:
    inc byte [ard_count]        ; finally we increase our array count
    ret                         ; and we return
;-------------------------------------------------------------------------------
; enables line A20
enable_a20:
    mov si, line_a20_msg
    call print
    mov ax, 0x2401
    int 0x15
    jc halt
    ret
;-------------------------------------------------------------------------------
; halts the system
halt:
    mov si, halt_msg
    call print
    jmp $

packet equ 0x7e00
stage1 equ 0x500        ; location where stage 1 would be loaded
stack_base equ 0x5c00   ; stack base address
stack_size equ 0x2000   ; stack size of 8K, may be an overkill
ard_count  equ 0x7e10   ; no. of ARD structs
ard_buffer equ 0x7e12   ; pointer to first ARD struct
ard_struct_size equ 20  ; ARD struct size; its 24 but we don't care about ACPI
smap equ 0x534d4150     ; string "smap"

load_msg db "b0: loaded", 0
line_a20_msg db "b0: enabling line a20", 0
sysmap_msg db "b0: smap -> 0x7e12", 0
halt_msg db ":(", 0
boot_drive dw 0

times 440 - ($ - $$) db 0
