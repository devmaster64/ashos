#ifndef TELETYPE_H

#define VGA_FB_ADDR 0xb8000
#define VGA_FB_ROWS 25
#define VGA_FB_COLS 80

#define REG_SCREEN_CTRL 0x3d4
#define REG_SCREEN_DATA 0x3d5

#include <types.h>

typedef struct vga_fb {
    u8 ascii_character;
    u8 color_data;
} vga_fb;

void print(str string);

#define TELETYPE_H
#endif