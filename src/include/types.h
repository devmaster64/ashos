#ifndef TYPES_H
#define TYPES_H
#define BITS_32 // TODO: Will change to 64 when build using amd64 gcc compiler

#define NULL 0

/* signed */
typedef char            i8;
typedef short int       i16;
typedef int             i32;
#ifdef BITS_32
typedef long long int   i64;
#else
typedef long int        i64;
#endif

/* unsigned */
typedef unsigned char          u8;
typedef unsigned short int     u16;
typedef unsigned int           u32;
#ifdef BITS_32
typedef unsigned long long int u64;
#else
typedef unsigned long int      u64;
#endif

/* string */
typedef unsigned char*         str;
#endif